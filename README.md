# Autentificación con Auth0 en Ionic

Auth0 proporciona autenticación y autorización como un servicio.

Se puede conectar cualquier aplicación (escrita en cualquier idioma) a Auth0 y definir los proveedores de identidad que desea usar (cómo desea que sus usuarios inicien sesión). Cada vez que un usuario intenta autenticarse, Auth0 verificará su identidad y enviará la información requerida a la aplicación conectada.

* **Para comenzar a usar este servicio es necesario registrarse en su página oficial** [**Auth0**](https://manage.auth0.com)

## Crear una nueva aplicación en Auth0
* Nos dirigimos a **Aplications**, seleccionamos **create aplication**

![imagen](imagenes/create-app.png)

* Se debe crear una aplicación de tipo **Native**, ademas, debemos añadir un nombre a la aplicación.

![imagen](imagenes/name-app.png)

## Obtener Application Keys

Una vez que se haya creado la aplicación, obtendremos la siguiente información, que es muy importante para usar el servicio.
* **Client ID**
* **Domain**

![imagen](imagenes/keys.png)

## Configuración de Callback URLs

Un callback URL es una URL en la aplicación donde Auth0 redirige al usuario después de que se haya autenticado.

El Callback URL se debe añadir a una lista de Callbacks URLs 

![imagen](imagenes/callbacks-URLs.png)

Además, el callback URL que se usará para la aplicación incluye el ID del paquete **(PACKAGE_ID)** de la aplicación, que se encuentra en el archivo config.xml de la aplicación

![image](imagenes/ID.png)

Para poder colocar el callback URL se usa el siguiente sentencia: 
```
TU_PACKAGE_ID://TU_DOMAIN/cordova/TU_PACKAGE_ID/callback
```
el **PACKAGE ID** se encuentra dentro del archivo **config.xml** y el  **DOMAIN** se encuentra en el dashboard de Auth0 de la aplicación.

Agregue el archivo **file** dentro de **Allowed Origins (CORS)**
```
file://*
```
![image](imagenes/cors.png)

## Instalación de Dependencias
Para el uso de Auth0 en Ionic es obligatorio instalar las siguientes dependencias.

```
npm install auth0-js @auth0/cordova --save
```

```
ionic cordova plugin add cordova-plugin-safariviewcontroller
```
para el siguiente se debe cambiar **YOUR_PACKAGE_ID** por el identificador de la aplicación, también **YOUR_DOMAIN**.
```
ionic cordova plugin add cordova-plugin-customurlscheme --variable URL_SCHEME={YOUR_PACKAGE_ID} --variable ANDROID_SCHEME={YOUR_PACKAGE_ID} --variable ANDROID_HOST=YOUR_DOMAIN --variable ANDROID_PATHPREFIX=/cordova/{YOUR_PACKAGE_ID}/callback
```

## Integración de Auth0 en la Aplicación

Se debe añadir la siguiente línea al archivo **config.xml**: 
```javascript
<preference name="AndroidLaunchMode" value="singleTask" />
```
![imagen](imagenes/config.png)

### Configuración Auth0

* Se debe crear un archivo que contenga el siguiente bloque de código:

```javascript
  export const AUTH_CONFIG = {
    clientID: 'YOUR_CLIENT_ID',  
    clientId: 'YOUR_CLIENT_ID',  
    domain: 'YOUR_DOMAIN',
    callbackURL: location.href,
    audience: 'https://YOUR_DOMAIN/api/v2/',
    packageIdentifier: 'YOUR_PACKAGE_ID'
};
```

![imagen](imagenes/config2.png)

* A continuación se debe crear archivo de servicio, en cual contendrá el siguiente bloque de código:

```javascript

import { Injectable, NgZone } from '@angular/core';
import Auth0Cordova from '@auth0/cordova';
import * as auth0 from 'auth0-js';
import { AUTH_CONFIG } from '../constantes/constantes';


@Injectable()
export class AuthService {
    auth0 = new auth0.WebAuth(AUTH_CONFIG);
    auth0Cordova = new Auth0Cordova(AUTH_CONFIG);
    auth0CordovaPaquete = Auth0Cordova;
    accessToken: string;
    idToken: string;
     user: any;

    constructor(
        public zone: NgZone,
    ) {
        this.user = this.getStorageVariable('profile');
        this.idToken = this.getStorageVariable('id_token');
    }

    private getStorageVariable(name) {
        return JSON.parse(window.localStorage.getItem(name));
    }

    private setStorageVariable(name, data) {
        window.localStorage.setItem(name, JSON.stringify(data));
    }

    private setIdToken(token) {
        this.idToken = token;
        this.setStorageVariable('id_token', token);
    }

    private setAccessToken(token) {
        this.accessToken = token;
        this.setStorageVariable('access_token', token);
    }

    public isAuthenticated() {
        const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
        return Date.now() < expiresAt;
    }

    login() {
        const options = {
            scope: 'openid profile offline_access',            
        };

        this.auth0Cordova.authorize(options, (err, auth0Result) => {
            if (err) {
                throw err;
            }

            this.setIdToken(auth0Result.idToken);
            this.setAccessToken(auth0Result.accessToken);
            const expiresAt = JSON.stringify((auth0Result.expiresIn * 1000) + new Date().getTime());
            this.setStorageVariable('expires_at', expiresAt);
            this.obtenerInformacionDelUsuarioLogeado(this.accessToken)
        });
    }

    obtenerInformacionDelUsuarioLogeado(accessToken: any) {
        this.auth0.client.userInfo(accessToken, (err, profile) => {
            if (err) {
                throw err;
            }
            profile.user_metadata = profile.user_metadata || {};
            this.setStorageVariable('profile', profile);
            this.zone.run(() => {
                this.user = profile;               
            });
        });
    }

    public logout() {
        window.localStorage.removeItem('profile');
        window.localStorage.removeItem('access_token');
        window.localStorage.removeItem('id_token');
        window.localStorage.removeItem('expires_at');
        this.idToken = null;
        this.accessToken = null;
        this.user = null;
    }

}
```
![imagen](imagenes/config3.png)

### Configuración URL de Redirección
* Se debe añadir el servicio en el app.module.ts, para poder usarlo.

![imagen](imagenes/config4.png)

* Para su uso nos dirigimos a app.component.ts, importamos el servicio que creamos de Auth0, en el constructor y agregamos la siguiente línea dentro del constructor

```javascript
(<any>window).handleOpenURL = (url) => {
        this._authService.auth0CordovaPaquete.onRedirectUri(url);
      };
```
![imagen](imagenes/config5.png)




<a href="https://twitter.com/kevi_n_24" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @kevi_n_24 </a><br>






